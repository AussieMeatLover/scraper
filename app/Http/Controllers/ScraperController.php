<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Goutte\Client;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ScraperController extends Controller
{
    public function input(Request $request)
    {
        //first, let's check user's input
        //if the input is empty, error returned to the page
        //if the input is not a valid url, error returned to the page
        $this->validator($request);

        //now get user input value
        $url = $request->url;

        //now, try to resolve the url
        try {
            $client = new Client();
            $crawler = $client->request('GET', $url);
        }catch (\Exception $exception){
            return Redirect::back()
                ->withErrors(['err_msg' => $exception->getMessage()])
                ->withInput($request->all());
        }

        //this means there is no table with wikitable class in the web page
        if ($crawler->filter('table.wikitable')->count() == 0){
            return Redirect::back()
                ->withErrors(['err_msg' => 'There is no proper data table found with the input URL.'])
                ->withInput($request->all());
        }

        //nextAll function here to ignore the first row of the datatable.
        //As the first row is the head of the table.
        $table_rows = $crawler->filter('table.wikitable tbody > tr')->nextAll();

        //pattern here defines what numeric value is acceptable.
        //pattern for extracting float or int numbers
        $pattern = '!\d+\.?\d+!';

        //new empty array to save results from the web page
        $result_from_web_page_array=[];

        //First loop through all rows
        foreach ($table_rows as $table_row){
            //And get all columns in a rwo
            $columns_for_each_row = $table_row->childNodes;
            for ($i = 0; $i < $columns_for_each_row->length; $i++) {
                $column_text =  $columns_for_each_row->item($i)->nodeValue;
                if(preg_match($pattern, $column_text,$matches)) {
                    array_push($result_from_web_page_array, $matches[0]);
                    break;
                }
            }
        }

        //need to do a minor change here.
        //To meet Google Line Graph input data requirement.
        $result_for_view_to_display = [];
        $i = 1;
        foreach ($result_from_web_page_array as $item){
            $result_for_view_to_display[$i]=$item;
            $i++;
        }

        //now need to check if the array is empty
        if (count($result_for_view_to_display) == 0){
            return Redirect::back()
                ->withErrors(['err_msg' => 'There is no valid data found in the table with the input URL.'])
                ->withInput($request->all());
        }

        //All good now, return the result to front view
        return view('preview',compact(['result_for_view_to_display','url']));
    }

    public function validator($request)
    {
        return Validator::make($request->all(),[
            'url' => 'required|url',
        ])->validate();
    }

}
