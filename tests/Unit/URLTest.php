<?php

namespace Tests\Unit;

use Goutte\Client;
use PHPUnit\Framework\TestCase;

class URLTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_url_without_valid_wikitable()
    {
        //https://en.wikipedia.org/wiki/Hoopoe_starling
        //The above link should not have any HTML table with 'wikitable' class
        $client = new Client();
        $crawler = $client->request('GET', 'https://en.wikipedia.org/wiki/Hoopoe_starling');
        $this->assertSame(0 , $crawler->filter('table.wikitable')->count());
    }
}
