<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class InputTest extends TestCase
{

    public function test_input_post_test()
    {
        $response = $this->call('post','input',[
            "url"=>"https://en.wikipedia.org/wiki/Women%27s_high_jump_world_record_progression"
        ]);
        $response->assertStatus(200);
    }


    public function test_no_input_post_test()
    {
        $response = $this->call('post','input',[

        ]);
        $response->assertStatus(302);
    }
}
