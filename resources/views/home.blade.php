@extends('layouts.app')
@section('content')
    <div class="container-lg pt-2 text-center">
        <form method="POST" action="{{ route('input') }}">
            @csrf
            <div class="form-group">
                <label for="url">Please input a link</label>
                <input id="url" type="text" name="url" class="form-control @error('url') is-invalid @enderror" value="{{ old('url') }}">
                <small id="emailHelp" class="form-text text-muted">Example Link:</small>
                <small id="emailHelp" class="form-text text-muted">https://en.wikipedia.org/wiki/Women%27s_high_jump_world_record_progression</small>
                <small id="emailHelp" class="form-text text-muted">https://en.wikipedia.org/wiki/Hoopoe_starling</small>
                <small id="emailHelp" class="form-text text-muted">https://en.wikipedia.org/wiki/List_of_NBA_champions</small>
                <small id="emailHelp" class="form-text text-muted">https://en.wikipediaaaaaaa.org/wiki/Women%27s_high_jump_world_record_progression</small>
                <small id="emailHelp" class="form-text text-muted">https://www.youtube.com/watch?v=Il0S8BoucSA&ab_channel=EdSheeran</small>
            </div>

            @if($errors->all())
                <div class="pt-2 alert alert-danger">{{$errors->first()}}</div>
            @endif

            <button type="submit" class="btn btn-primary">
                Submit
            </button>
        </form>
    </div>
@endsection
