@extends('layouts.app')
@section('content')
    <div id="curve_chart" ></div>

    <div class="pt-1 pb-1 container text-center">
        <button id="generateImage" type="button" class="btn btn-dark">Generate Image File In New Tab</button>
    </div>

    <div class="pt-1 container">
        <div class="alert alert-primary" role="alert">
            Values got from: <a target="_blank" href={{ $url }}>{{ $url }}</a>
        </div>
        <table style="background-color: #ffffff" class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Value</th>
            </tr>
            </thead>
            <tbody>
            @foreach($result_for_view_to_display as $k=>$v)
                <tr>
                    <th scope="row">{{ $k}}</th>
                    <td>{{ $v }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var result_data = @json($result_for_view_to_display);
            var result = Object.keys(result_data).map((key) => [key, parseFloat(result_data[key])]);
            var title_for_google_chart = [['Index', 'Value']] ;
            var final = title_for_google_chart.concat(result)

            var data = google.visualization.arrayToDataTable(final);


            var options = {
                legend: { position: 'bottom' }
            };

            var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

            chart.draw(data, options);
        }
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
    <script>
        $(document).ready(function(){

            var element = $('.table');

            $('#generateImage').on('click', function(){
                html2canvas(element, {
                    background: '#ffffff',
                    onrendered: function(canvas){
                        var imgData = canvas.toDataURL('image/jpeg');
                        let w = window.open('about:blank');
                        let image = new Image();
                        image.src = imgData;
                        w.document.write(image.outerHTML);
                        console.log(imgData);
                    }
                });
            });

        });
    </script>

@endsection

